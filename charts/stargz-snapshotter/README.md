# Stargz Snapshotter

Prerequisites:
 * containerd with at least [commit `d8506bf`](https://github.com/containerd/containerd/commit/d8506bfd7b407dcb346149bcec3ed3c19244e3f1) which means version v1.4.0-beta.0 or greater.

## Installation

```console
helm repo add cern http://registry.cern.ch/chartrepo/cern
helm install cern/stargz-snapshotter
```
